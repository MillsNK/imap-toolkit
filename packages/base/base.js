let _DATETIMEFORMAT = "DD/MM/YYYY HH:mm";
let _MILDATETIMEFORMAT = "DDHHmmMMMYY";
let _DATEFORMAT = "dd/mm/yy";
var _PLANNINGSTEPSFILEPATH = "/data/planningsteps.txt";
let _PLANNINGSTEPS = readTextFile(_PLANNINGSTEPSFILEPATH);
var _DISTRIBUTIONLISTFILEPATH = "/data/distributionlist.txt";
let _DISTRIBUTIONLIST = readTextFile(_DISTRIBUTIONLISTFILEPATH);
let _GOOGLEMAPSAPIKEY = config.GOOGLE_MAPS_API_KEY;
let _SYMBOLOPTIONSFILEPATH = "/data/symboloptions.txt";
let _SYMBOLOPTIONS = readTextFile(_SYMBOLOPTIONSFILEPATH);
let _SYMBOLMODIFIER1FILEPATH = "/data/symbolModifier1.txt";
let _SYMBOLMODIFIER1 = readTextFile(_SYMBOLMODIFIER1FILEPATH);
let _SYMBOLMODIFIER2FILEPATH = "/data/symbolModifier2.txt";
let _SYMBOLMODIFIER2 = readTextFile(_SYMBOLMODIFIER2FILEPATH);
let _SYMBOLAFFILIATIONFILEPATH = "/data/symbolaffiliation.txt";
let _SYMBOLAFFILIATION = readTextFile(_SYMBOLAFFILIATIONFILEPATH);
let _SYMBOLSTATUSFILEPATH = "/data/symbolstatus.txt";
let _SYMBOLSTATUS = readTextFile(_SYMBOLSTATUSFILEPATH);

/* readTextFile
 * Open the JSON file at the _FILEPATH global variable and read in.
 * This file holds the JSON definition for site settings (WSOCS and RMCE nodes).
 */
function readTextFile(file)
{
    var rawFile = new XMLHttpRequest();
	var allText;
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function () {
        if(rawFile.readyState === 4) {
            if(rawFile.status === 200 || rawFile.status == 0) {
                allText = rawFile.responseText;
            }
        }
    }
    rawFile.send(null);
	return JSON.parse(allText);
}

/* addRow
 * For the defined table, this duplicates the first row and adjust the td element id's to ensure
 * they are unique.
 */
function addRow(table, rowId) {
	console.log('addRow');
	
	var row = document.getElementById(rowId+"[0]");
	
	var lastId = document.getElementById(table).getElementsByTagName("tr").length-3;
	console.log(lastId);
	var newId = lastId+1;
	
	// Copy the element and its child nodes
	var clone = row.cloneNode(true);
	clone.id = rowId+'['+newId+']';
	clone.style.visibility = "visible";

	var els = clone.getElementsByTagName('td');
	for (var i = 0; i < els.length; i++) {
		var elId = els[i].children[0].id;
		var elIdRoot = elId.split('[');
		var elNewId = elIdRoot[0]+'['+newId+']';
		// If the item has a datepicker, destroy it
		if (els[i].children[0].classList.contains("hasDatepicker")) {
			els[i].removeChild(els[i].children[0]);
			var input = document.createElement("input");
			input.className = "form-control form-control-sm datetimepicker";
			els[i].appendChild(input);
			els[i].children[0].id = elNewId;
		}
		else {
			els[i].children[0].id = elNewId;
		}
	}

	// Append the cloned element
	document.getElementById(table).tBodies[0].appendChild(clone);
	return newId;
}

/* removeRow
 * For the defined table, this removes the last row of the table
 */
function removeRow(table, rowId) {
	console.log('removeClicked');
	
	var lastId = document.getElementById(table).getElementsByTagName("tr").length-3;
	
	if (lastId != 0) {
		var element = document.getElementById(rowId+"["+lastId+"]");
		element.outerHTML = "";
		delete element;
	}
}

/* clearTable
 * Clears table rows delivered to function
 */
function clearTable(table) {
	console.log("clearTable");
	var rows = table.rows;
	var i = rows.length-1;
	console.log(i);
	while (i>2) {
		rows[i].parentNode.removeChild(rows[i]);
		i--;
	}
}

function addEscapeChars(str) {
	if (str.indexOf("[") >= 0) {
		str = str.splice(str.indexOf("["), 0, "\\");
	}
	if (str.indexOf("]") >= 0) {
		str = str.splice(str.indexOf("]"), 0, "\\")
	}
	return str;
}

function buildSymbol(sidc, additionalOptions, canvasName) {
	console.log(canvasName);
	if (additionalOptions != null) {
		if (additionalOptions[Object.keys(additionalOptions)[0]] != "") {
			if (additionalOptions.dtg != undefined) {
				additionalOptions.dtg = moment(additionalOptions.dtg, _DATETIMEFORMAT).format(_MILDATETIMEFORMAT);
			}
			Object.keys(additionalOptions).forEach(function (key) {
				additionalOptions[key] = additionalOptions[key].toUpperCase();
			});
		}
	}
	
	var marker = new ms.Symbol(sidc, additionalOptions);
	var canvasMarker = marker.asCanvas();
	
	var img = canvasMarker.toDataURL("image/png");
	var elem = document.createElement("img");
	elem.src = img;
	//document.getElementById(canvasName).appendChild(elem);
	return elem;
}

function buildSymbol2(sidc, additionalOptions) {
	if (additionalOptions != null) {
		if (additionalOptions[Object.keys(additionalOptions)[0]] != "") {
			if (additionalOptions.dtg != undefined) {
				additionalOptions.dtg = moment(additionalOptions.dtg, _DATETIMEFORMAT).format(_MILDATETIMEFORMAT);
			}
			Object.keys(additionalOptions).forEach(function (key) {
				additionalOptions[key] = additionalOptions[key].toUpperCase();
			});
		}
	}
	
	var marker = new ms.Symbol(sidc, additionalOptions);
	var canvasMarker = marker.asCanvas();
	console.log(canvasMarker);
	return canvasMarker.toDataURL("image/png");
}

function addOption(itemName, itemValue, select) {
		var option = document.createElement("option");
		option.text = itemName;
		option.value = itemValue;
		document.getElementById(select).add(option);
}

function getElement(data, parameter, paramName) {
  return data.filter(
    function(data) {
		return data[paramName] == parameter
    }
  );
}