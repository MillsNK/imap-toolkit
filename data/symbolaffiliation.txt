[
	{
		"value": "P",
		"name": "Pending"
	},
	{
		"value": "U",
		"name": "Unknown"
	},
	{
		"value": "A",
		"name": "Assumed Friend"
	},
	{
		"value": "F",
		"name": "Friend"
	},
	{
		"value": "N",
		"name": "Neutral"
	},
	{
		"value": "S",
		"name": "Suspect"
	},
	{
		"value": "H",
		"name": "Hostile"
	},
	{
		"value": "G",
		"name": "Exercise Pending"
	},
	{
		"value": "W",
		"name": "Exercise Unknown"
	},
	{
		"value": "D",
		"name": "Exercise Friend"
	},
	{
		"value": "L",
		"name": "Exercise Neutral"
	},
	{
		"value": "M",
		"name": "Exercise Assumed Friend"
	},
	{
		"value": "J",
		"name": "Joker"
	},
	{
		"value": "K",
		"name": "Faker"
	},
	{
		"value": "O",
		"name": "None Specified"
	}
]